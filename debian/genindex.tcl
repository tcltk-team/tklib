#!/usr/bin/tclsh

set docdir [lindex $argv 0]
set manuals {}

foreach docfile [glob -nocomplain -directory $docdir *.html] {
    puts stderr $docfile

    if {[catch {set fd [open $docfile]} msg]} {
	puts stderr "Can't open file $docfile: $msg"
	continue
    }

    while {[gets $fd line] >= 0} {
	if {[regexp {<h1[^>]*>\s*(.*)\(\S*\)\s+(\S+)\s+(\S+)\s+(.*)</h1>} $line -> \
		    name version module title]} {
	    lappend manuals [list $module!$title!$name $module $name $version $title [file tail $docfile]]
	    break
	}
    }
    close $fd
}

puts "<html>
<head>
<title>Tklib HTML Documentation</title>
<style type=\"text/css\">
<!--
ul {
    background: lightyellow;
    border-style: solid;
    border-width: 1px;
    border-color: black;
}
li {
    padding: 3px;
}
li a {
    font-weight: bold;
}
-->
</style>
</head>
<body>
<h1>Tklib HTML Documentation</h1>"

set manuals [lsort -index 0 $manuals]
set prev ""
foreach m $manuals {
    set module  [lindex $m 1]
    set name    [lindex $m 2]
    set version [lindex $m 3]
    set title   [lindex $m 4]
    set fname   [lindex $m 5]

    if {$module != $prev} {
	if {$prev != ""} {
	    puts "</ul>"
	}
	puts "<h2>$module</h2>\n<ul>"
	set prev $module
    }
    puts "<li><a href=\"$fname\">$name</a> $version $title</li>"
}
puts "
</ul>
<h2>tablelist</h2>
<ul>
<li><a href=\"tablelist/index.html\">tablelist</a> - &quot;A multicolumn listbox
package&quot;</li>
</ul>
</body>
</html>"

